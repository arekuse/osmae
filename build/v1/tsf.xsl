<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <body>
  <h2><xsl:value-of select="tsf/name"/></h2>
  <h4><xsl:value-of select="tsf/description"/></h4>
  <table border="1" width="100%">
    <tr bgcolor="#1E8F03">
      <th>Control</th>
      <th>Step Nr</th>
      <th>PRECONDITION</th>
    </tr>
    <xsl:for-each select="tsf/precondition/step">
    <tr>
      <td width="5%"><input type='checkbox'/></td>
      <td width="5%"><xsl:value-of select="@id"/></td>
      <td width="90%"><xsl:value-of select="."/></td>
    </tr>
  </xsl:for-each>
  </table>
  <br/>
  <table border="1" width="100%">
    <tr bgcolor="#FFB600">
      <th>Control</th>
      <th>Step Nr</th>
      <th>ACTION</th>
    </tr>
    <xsl:for-each select="tsf/action/step">
    <tr>
      <td width="5%"><input type='checkbox'/></td>
      <td width="5%"><xsl:value-of select="@id"/></td>
      <td width="90%"><xsl:value-of select="."/></td>
</tr>
    </xsl:for-each>
  </table>
  <br/>
  <table border="1" width="100%">
    <tr bgcolor="#1E8F03">
      <th>Control</th>
      <th>Step Nr</th>
      <th>POSTCONDITION</th>
    </tr>
    <xsl:for-each select="tsf/postcondition/step">
    <tr>
      <td width="5%"><input type='checkbox'/></td>
      <td width="5%"><xsl:value-of select="@id"/></td>
      <td width="90%"><xsl:value-of select="."/></td>
    </tr>
    </xsl:for-each>
  </table>
  <br/>
  <table border="1" width="100%">
    <tr bgcolor="#FF3333">
      <th>Control</th>
      <th>Step Nr</th>
      <th>EXPECTED RESULT</th>
    </tr>
    <xsl:for-each select="tsf/expected_result/step">
    <tr>
      <td width="5%"><input type='checkbox'/></td>
      <td width="5%"><xsl:value-of select="@id"/></td>
      <td width="90%"><xsl:value-of select="."/></td>
    </tr>
    </xsl:for-each>
  </table>
  </body>
  </html>
</xsl:template>

</xsl:stylesheet>