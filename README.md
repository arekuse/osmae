Open Specification Manager and Editor

This tool allows you creating test specifications as text or xml files using a dictionary of expressions.

Source Code in Object Pascal using Lazarus IDE

TODO:
- default dictionary need to have more examples
- example specification files (tsf and tsfx)
- hide internal pages in the app
- new icon

Quick Usage
- copy contents from build/v1 to c:\temp and you can run the exe-file
- adjust default.xml to have more functions (no editor yet...)
- create new specifications and add functions to precondition, action, postcondition and expected result with step numbers from the loaded dictionary or manually
- generate the specification in the overview
- save the specification as a text file (tsf) or an xml (tsfx)
- reload the specification file or check it in the project overview
