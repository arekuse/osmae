unit project_manager;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, FileCtrl,
  ComCtrls;

type

  { Tproject_manager_form }

  Tproject_manager_form = class(TForm)
    Button1: TButton;
    FileListBox1: TFileListBox;
    StatusBar1: TStatusBar;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

  public

  end;

var
  project_manager_form: Tproject_manager_form;

implementation

uses osmae_src;

{$R *.lfm}

{ Tproject_manager_form }

procedure Tproject_manager_form.FormCreate(Sender: TObject);
begin
  filelistbox1.Directory:=ExtractFilePath(ParamStr(0))+'projects';
end;

procedure Tproject_manager_form.Button1Click(Sender: TObject);
begin

  statusbar1.SimpleText:=filelistbox1.FileName+'\'+extractfilename(filelistbox1.FileName)+'.osmp';
  if fileexists(statusbar1.SimpleText) then
     begin
     osmae_src.Form1.Show;
     osmae_src.form1.load_project(statusbar1.SimpleText);
     project_manager_form.hide;
     end
  else
    showmessage('No project is found');
end;

end.

