unit osmae_src;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  ComCtrls, Menus, EditBtn, Grids, FileCtrl, HtmlView, Types, DOM, XMLRead,
  XMLWrite, XPath, lazutf8, SynEdit, SynCompletion, SynPluginSyncroEdit,
  SynHighlighterAny, character, StrUtils, regexpr, FileUtil,
  SynEditMarkupHighAll, SynHighlighterXML, SynExportHTML, IpHtml, Ipfilebroker, process;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    ComboBox1: TComboBox;
    Edit4: TEdit;
    EditButton1: TEditButton;
    GroupBox10: TGroupBox;
    GroupBox11: TGroupBox;
    GroupBox12: TGroupBox;
    GroupBox8: TGroupBox;
    GroupBox9: TGroupBox;
    HtmlViewer1: THtmlViewer;
    Label4: TLabel;
    LabeledEdit1: TLabeledEdit;
    LabeledEdit2: TLabeledEdit;
    LabeledEdit3: TLabeledEdit;
    LabeledEdit4: TLabeledEdit;
    ListBox4: TListBox;
    MenuItem10: TMenuItem;
    MenuItem11: TMenuItem;
    MenuItem12: TMenuItem;
    MenuItem13: TMenuItem;
    MenuItem14: TMenuItem;
    MenuItem15: TMenuItem;
    MenuItem16: TMenuItem;
    MenuItem17: TMenuItem;
    MenuItem18: TMenuItem;
    MenuItem19: TMenuItem;
    MenuItem20: TMenuItem;
    MenuItem21: TMenuItem;
    MenuItem22: TMenuItem;
    MenuItem23: TMenuItem;
    MenuItem24: TMenuItem;
    MenuItem25: TMenuItem;
    MenuItem26: TMenuItem;
    MenuItem27: TMenuItem;
    MenuItem28: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem30: TMenuItem;
    MenuItem31: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    MenuItem8: TMenuItem;
    MenuItem9: TMenuItem;
    OpenDialog1: TOpenDialog;
    OpenDialog2: TOpenDialog;
    OpenDialog3: TOpenDialog;
    operations_groups: TComboBox;
    expressions_groups: TComboBox;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    operations_filter: TEditButton;
    expressions_filter: TEditButton;
    FileListBox1: TFileListBox;
    FileListBox2: TFileListBox;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    GroupBox5: TGroupBox;
    GroupBox6: TGroupBox;
    GroupBox7: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    desc: TMemo;
    PageControl3: TPageControl;
    PopupMenu1: TPopupMenu;
    SaveDialog1: TSaveDialog;
    SaveDialog2: TSaveDialog;
    Splitter5: TSplitter;
    Splitter6: TSplitter;
    SynAnySyn1: TSynAnySyn;
    SynXMLSyn1: TSynXMLSyn;
    TabSheet10: TTabSheet;
    TabSheet11: TTabSheet;
    TabSheet12: TTabSheet;
    TabSheet9: TTabSheet;
    temp: TMemo;
    dict_temp: TMemo;
    pre_list: TListBox;
    act_list: TListBox;
    post_list: TListBox;
    exp_list: TListBox;
    operations: TListBox;
    expressions: TListBox;
    ListBox8: TListBox;
    MainMenu1: TMainMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    PageControl1: TPageControl;
    PageControl2: TPageControl;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    Splitter3: TSplitter;
    Splitter4: TSplitter;
    StatusBar1: TStatusBar;
    StatusBar2: TStatusBar;
    StatusBar3: TStatusBar;
    StringGrid1: TStringGrid;
    SynEdit1: TSynEdit;
    overview: TSynEdit;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    TabSheet6: TTabSheet;
    TabSheet7: TTabSheet;
    TabSheet8: TTabSheet;
    procedure act_listContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure act_listDblClick(Sender: TObject);
    procedure act_listDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure act_listDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure act_listKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure act_listSelectionChange(Sender: TObject; User: boolean);
    procedure act_listStartDrag(Sender: TObject; var DragObject: TDragObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure descEditingDone(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: char);
    procedure EditButton1ButtonClick(Sender: TObject);
    procedure EditButton1Change(Sender: TObject);
    procedure expressionsDblClick(Sender: TObject);
    procedure expressionsKeyPress(Sender: TObject; var Key: char);
    procedure expressionsKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure expressionsSelectionChange(Sender: TObject; User: boolean);
    procedure expressionsStartDrag(Sender: TObject; var DragObject: TDragObject
      );
    procedure expressions_filterButtonClick(Sender: TObject);
    procedure expressions_filterChange(Sender: TObject);
    procedure expressions_filterKeyPress(Sender: TObject; var Key: char);
    procedure exp_listContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure exp_listDblClick(Sender: TObject);
    procedure exp_listDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure exp_listDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure exp_listKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure exp_listSelectionChange(Sender: TObject; User: boolean);
    procedure exp_listStartDrag(Sender: TObject; var DragObject: TDragObject);
    procedure FileListBox1Change(Sender: TObject);
    procedure FileListBox1SelectionChange(Sender: TObject; User: boolean);
    procedure FileListBox2Change(Sender: TObject);
    procedure FileListBox2SelectionChange(Sender: TObject; User: boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure GroupBox5DblClick(Sender: TObject);
    procedure LabeledEdit1Change(Sender: TObject);
    procedure LabeledEdit4Change(Sender: TObject);
    procedure ListBox8DblClick(Sender: TObject);
    procedure ListBox8ShowHint(Sender: TObject; HintInfo: PHintInfo);
    procedure MenuItem10Click(Sender: TObject);
    procedure MenuItem11Click(Sender: TObject);
    procedure MenuItem13Click(Sender: TObject);
    procedure MenuItem14Click(Sender: TObject);
    procedure MenuItem21Click(Sender: TObject);
    procedure MenuItem30Click(Sender: TObject);
    procedure MenuItem19Click(Sender: TObject);
    procedure MenuItem22Click(Sender: TObject);
    procedure MenuItem23Click(Sender: TObject);
    procedure MenuItem24Click(Sender: TObject);
    procedure MenuItem25Click(Sender: TObject);
    procedure MenuItem28Click(Sender: TObject);
    procedure MenuItem29Click(Sender: TObject);
    procedure MenuItem5Click(Sender: TObject);
    procedure MenuItem6Click(Sender: TObject);
    procedure MenuItem8Click(Sender: TObject);
    procedure operationsSelectionChange(Sender: TObject; User: boolean);
    procedure operations_filterButtonClick(Sender: TObject);
    procedure operations_filterChange(Sender: TObject);
    procedure operations_filterDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure operations_filterKeyPress(Sender: TObject; var Key: char);
    procedure PageControl2ContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure post_listContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure MenuItem2Click(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
    procedure MenuItem4Click(Sender: TObject);
    procedure operations_groupsChange(Sender: TObject);
    procedure post_listDblClick(Sender: TObject);
    procedure post_listDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure post_listDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure post_listKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState
      );
    procedure post_listSelectionChange(Sender: TObject; User: boolean);
    procedure post_listStartDrag(Sender: TObject; var DragObject: TDragObject);
    procedure pre_listContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure pre_listDblClick(Sender: TObject);
    procedure pre_listDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure pre_listDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure pre_listKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure pre_listSelectionChange(Sender: TObject; User: boolean);
    procedure pre_listStartDrag(Sender: TObject; var DragObject: TDragObject);
    procedure Splitter2Moved(Sender: TObject);
  private
        FList: TStringList;
    FListExpr: TStringList;
    FListMemory: TStringList;
  public
    procedure load_project(project: String);
  end;

var
  Form1: TForm1;
  pre: array of string;
  act: array of string;
  post: array of string;
  res: array of string;
  exp: string;
  Xml: TXMLDocument;
  Xml_TSFX: TXMLDocument;
  Xml_Project: TXMLDocument;
  XPathResult: TXPathVariable;
  line_sep_marker: string = '.';
  draggedindex:integer;

  //project information
  osmp_author: string;
  osmp_dict: string;
  osmp_project: string;
  osmp_separator: string;
implementation

uses editor, bbutils, project_manager;

{$R *.lfm}
{ TForm1 }

procedure duplicate_item(Listbox: TListbox; pos: integer);
begin
if Listbox.selcount=1 then
begin
listbox.items.insert(pos, listbox.Items[listbox.ItemIndex]);
listbox.Selected[pos]:=true;
end;
end;

procedure move_item(Listbox1: TListbox; Listbox2: TListbox);
begin
if Listbox1.selcount=1 then
begin
listbox2.items.insert(listbox2.items.count, listbox1.Items[listbox1.ItemIndex]);
listbox1.items.Delete(listbox1.ItemIndex);
//listbox2.Selected[listbox2.items.count]:=true;
end;
end;

function get_step_pos(input: string; divider: string): string;
var number: integer;
begin
if not TryStrToInt(utf8copy(input, 1, utf8pos(divider, input)-1),number) then result:='' else result:=inttostr(number);
end;

procedure clear_editor;
begin
  with form1 do
begin
overview.Clear;
pre_list.Clear;
act_list.clear;
post_list.clear;
exp_list.clear;
desc.clear;
labelededit1.Clear;
labelededit2.Clear;
labelededit3.Clear;
//labelededit4.Clear;
pagecontrol2.ActivePageIndex:=0;
statusbar1.Panels[3].Text:='Name:';
statusbar1.Panels[4].Text:='File:';
//dict_temp.clear;
end;
end;

function get_op_name(input: string): string;
var op_name: string = '';
begin
op_name:=strSplit(input,'|')[0];
result:=op_name;
end;

function get_op_group(input: string): string;
var op_group: string = '';
begin
op_group:=strSplit(input,'|')[1];
result:=op_group;
end;

procedure read_dict(xml_file: string);
var i, j, k, l : integer;
  funct_num, expr_num, alias_num, bus_num, ecu_num: integer;
  group_num, op_num: integer;
  op_name, group_name: string;
begin
with form1 do
    begin
        ReadXMLFile(Xml, xml_file);
        operations.Clear;
        operations_groups.Clear;
        expressions.Clear;
        dict_temp.clear;
        //count groups
        group_num := strtoint(String(EvaluateXPathExpression('count(//group)', Xml.DocumentElement).AsText));
        //load groups
        for i:=0 to group_num-1 do
             operations_groups.Items.Add(String(EvaluateXPathExpression('//group['+inttostr(i+1)+']/@name', Xml.DocumentElement).AsText));
        //count operations
        op_num := strtoint(String(EvaluateXPathExpression('count(//function)', Xml.DocumentElement).AsText));
        //load operations
        for j:=0 to op_num-1 do
             begin
                  op_name:=String(EvaluateXPathExpression('//function['+inttostr(j+1)+']/@name', Xml.DocumentElement).AsText);
                  operations.items.add(op_name);
                  //count expressions
                  expr_num := strtoint(String(EvaluateXPathExpression('count(//function['+inttostr(j+1)+']//expression)', Xml.DocumentElement).AsText));
                  //load exoressions
                           for k:=0 to expr_num-1 do
                                    expressions.items.add(EvaluateXPathExpression('//function['+inttostr(j+1)+']//expression['+inttostr(k+1)+']', Xml.DocumentElement).AsText);
             end;
        expressions.Sorted:=true;
        operations.Sorted:=true;
        FList.Assign(operations.items);
        FListExpr.Assign(expressions.items);
        statusbar2.SimpleText:=inttostr(operations.Items.Count) + ' operation(s)';
        statusbar3.SimpleText:=inttostr(expressions.Items.Count) + ' expression(s)';
    end;
end;

procedure unselect_all(Lbox: TListBox);
var i: integer;
begin
for i:=0 to Lbox.Items.count-1 do
Lbox.Selected[i]:=false;
end;

function generate_line(line: integer): string;
begin
  if line <> 0 then
  result := inttostr(line) + line_sep_marker +' '
  else
  result := '';
end;

function get_step_text(input: string; divider: string): string;
begin
result:=utf8copy(input, utf8pos(divider, input)+1, utf8length(input));
end;

procedure save_xml(filename: String);
var
  Doc: TXMLDocument;
  RootNode, NewNode, NewStep, NewAttribute :TDomNode;
  i: integer;
begin
with form1 do
  begin
    Doc := TXMLDocument.Create;

    RootNode:=Doc.CreateElement('tsf');
    Doc.AppendChild(RootNode);

    NewNode:=doc.CreateElement('name');
    TDOMElement(NewNode).TextContent:=trim(labelededit1.Text);
    RootNode.AppendChild(NewNode);

    NewNode:=doc.CreateElement('description');
    TDOMElement(NewNode).TextContent:=trim(desc.Text);
    RootNode.AppendChild(NewNode);

    NewNode:=doc.CreateElement('mode');
    TDOMElement(NewNode).TextContent:=trim(labelededit2.Text);
    RootNode.AppendChild(NewNode);

    NewNode:=doc.CreateElement('layer');
    TDOMElement(NewNode).TextContent:=trim(labelededit3.Text);
    RootNode.AppendChild(NewNode);

    NewNode:=doc.CreateElement('separator');
    TDOMElement(NewNode).TextContent:=trim(labelededit4.Text);
    RootNode.AppendChild(NewNode);

//pre
    NewNode:=doc.CreateElement('precondition');
    for i:=0 to pre_list.Items.count-1 do
    begin
    NewStep:=doc.CreateElement('step');
    if get_step_pos(pre_list.items[i],line_sep_marker)<>'' then
    TDOMElement(NewStep).SetAttribute('id',get_step_pos(pre_list.items[i],line_sep_marker));
    TDOMElement(NewStep).TextContent:=trim(get_step_text(pre_list.items[i],line_sep_marker));
    NewNode.AppendChild(NewStep);
    end;
    RootNode.AppendChild(NewNode);
//act
     NewNode:=doc.CreateElement('action');
     for i:=0 to act_list.Items.count-1 do
     begin
     NewStep:=doc.CreateElement('step');
     if get_step_pos(act_list.items[i],line_sep_marker)<>'' then
     TDOMElement(NewStep).SetAttribute('id',get_step_pos(act_list.items[i],line_sep_marker));
     TDOMElement(NewStep).TextContent:=trim(get_step_text(act_list.items[i],line_sep_marker));
     NewNode.AppendChild(NewStep);
     end;
     RootNode.AppendChild(NewNode);
//post
     NewNode:=doc.CreateElement('postcondition');
     for i:=0 to post_list.Items.count-1 do
     begin
     NewStep:=doc.CreateElement('step');
     if get_step_pos(post_list.items[i],line_sep_marker)<>'' then
     TDOMElement(NewStep).SetAttribute('id',get_step_pos(post_list.items[i],line_sep_marker));
     TDOMElement(NewStep).TextContent:=trim(get_step_text(post_list.items[i],line_sep_marker));
     NewNode.AppendChild(NewStep);
     end;
     RootNode.AppendChild(NewNode);
//act
     NewNode:=doc.CreateElement('expected_result');
     for i:=0 to exp_list.Items.count-1 do
     begin
     NewStep:=doc.CreateElement('step');
     if get_step_pos(exp_list.items[i],line_sep_marker)<>'' then
     TDOMElement(NewStep).SetAttribute('id',get_step_pos(exp_list.items[i],line_sep_marker));
     TDOMElement(NewStep).TextContent:=trim(get_step_text(exp_list.items[i],line_sep_marker));
     NewNode.AppendChild(NewStep);
     end;
     RootNode.AppendChild(NewNode);
//write XML
    WriteXMLFile(Doc,filename);
    Doc.Free;
  end;
end;

procedure load_xml(filename: string);
var elem_count, i: integer;
  elem_temp: string;
begin
with form1 do
  begin
  clear_editor;
  ReadXMLFile(Xml_TSFX, fileName);
  labelededit1.text:=EvaluateXPathExpression('/tsf/name', Xml_TSFX.DocumentElement).AsText;
  statusbar1.Panels[3].Text:='Name: '+labelededit1.text;
  labelededit2.text:=EvaluateXPathExpression('/tsf/mode', Xml_TSFX.DocumentElement).AsText;
  labelededit3.text:=EvaluateXPathExpression('/tsf/layer', Xml_TSFX.DocumentElement).AsText;
  desc.text:=EvaluateXPathExpression('/tsf/description', Xml_TSFX.DocumentElement).AsText;
  labelededit4.text:=EvaluateXPathExpression('/tsf/separator', Xml_TSFX.DocumentElement).AsText;
  line_sep_marker:=labelededit4.text;
  statusbar1.Panels[1].Text:='Separator: '+line_sep_marker;
  elem_temp:=EvaluateXPathExpression('/tsf/separator', Xml_TSFX.DocumentElement).AsText;
  //showmessage(elem_temp);
  if trim(elem_temp)<>'' then line_sep_marker:=elem_temp;
  //pre count
  elem_count:=round(EvaluateXPathExpression('count(//precondition//step)', Xml_TSFX.DocumentElement).AsNumber);
  for i:=1 to elem_count do
  begin
  elem_temp:=String(EvaluateXPathExpression('//precondition/step['+inttostr(i)+']/@id', Xml_TSFX.DocumentElement).AsText);
  if elem_temp <> '' then elem_temp:=elem_temp + line_sep_marker+' '+ String(EvaluateXPathExpression('//precondition/step['+inttostr(i)+']', Xml_TSFX.DocumentElement).AsText)
  else elem_temp:=String(EvaluateXPathExpression('//precondition/step['+inttostr(i)+']', Xml_TSFX.DocumentElement).AsText);
  pre_list.Items.add(elem_temp);
  end;
  //act count
  elem_count:=round(EvaluateXPathExpression('count(//action//step)', Xml_TSFX.DocumentElement).AsNumber);
  for i:=1 to elem_count do
  begin
  elem_temp:=String(EvaluateXPathExpression('//action/step['+inttostr(i)+']/@id', Xml_TSFX.DocumentElement).AsText);
  if elem_temp<>'' then elem_temp:=elem_temp +line_sep_marker+' '+ String(EvaluateXPathExpression('//action/step['+inttostr(i)+']', Xml_TSFX.DocumentElement).AsText)
  else elem_temp:=String(EvaluateXPathExpression('//action/step['+inttostr(i)+']', Xml_TSFX.DocumentElement).AsText);
  act_list.Items.add(elem_temp);
  end;
  //post count
  elem_count:=round(EvaluateXPathExpression('count(//postcondition//step)', Xml_TSFX.DocumentElement).AsNumber);
  for i:=1 to elem_count do
  begin
  elem_temp:=String(EvaluateXPathExpression('//postcondition/step['+inttostr(i)+']/@id', Xml_TSFX.DocumentElement).AsText);
  if elem_temp<>'' then elem_temp:=elem_temp +line_sep_marker+' '+ String(EvaluateXPathExpression('//postcondition/step['+inttostr(i)+']', Xml_TSFX.DocumentElement).AsText)
  else elem_temp:=String(EvaluateXPathExpression('//postcondition/step['+inttostr(i)+']', Xml_TSFX.DocumentElement).AsText);
  post_list.Items.add(elem_temp);
  end;
  //exp count
  elem_count:=round(EvaluateXPathExpression('count(//expected_result//step)', Xml_TSFX.DocumentElement).AsNumber);
  for i:=1 to elem_count do
  begin
  elem_temp:=String(EvaluateXPathExpression('//expected_result/step['+inttostr(i)+']/@id', Xml_TSFX.DocumentElement).AsText);
  if elem_temp<>'' then elem_temp:= elem_temp +line_sep_marker+' '+ String(EvaluateXPathExpression('//expected_result/step['+inttostr(i)+']', Xml_TSFX.DocumentElement).AsText)
  else elem_temp:=String(EvaluateXPathExpression('//expected_result/step['+inttostr(i)+']', Xml_TSFX.DocumentElement).AsText);
  exp_list.Items.add(elem_temp);
  end;
  menuitem30.Click;
  end;
end;

procedure load_txt;
var i: integer;
  temp_step: integer;
begin
with form1 do
  begin
  clear_editor;
  temp.Lines.LoadFromFile(opendialog1.FileName);
  for i := 0 to temp.Lines.Count-1 do
      begin

           if AnsiStartsStr('PRECONDITION', uppercase(trim(temp.lines[i]))) then
             begin

             temp_step:=i;

                  while not(AnsiStartsStr('ACTION',uppercase(trim(temp.lines[temp_step+1])))) or (temp_step < temp.lines.count-1) do
                      begin
                      temp_step:=temp_step+1;
                           if trim(temp.lines[temp_step])<>'' then
                      pre_list.Items.Add(temp.lines[temp_step]);
                      end;

             temp_step:=-1;
             while not(AnsiStartsStr('PRECONDITION',uppercase(trim(temp.lines[temp_step+1])))) or (temp_step < temp.lines.count-1) do
                 begin
                 temp_step:=temp_step+1;
                      if trim(temp.lines[temp_step])<>'' then
                 desc.lines.Add(temp.lines[temp_step]);
                 end;
             end;
           if AnsiStartsStr('ACTION', uppercase(trim(temp.lines[i]))) then
             begin
             temp_step:=i;
                 while not(AnsiStartsStr('POSTCONDITION',uppercase(trim(temp.lines[temp_step+1])))) or (temp_step < temp.lines.count-1) do
                      begin
                      temp_step:=temp_step+1;
                           if trim(temp.lines[temp_step])<>'' then
                      act_list.Items.Add(temp.lines[temp_step]);
                      end;
             end;
           if AnsiStartsStr('POSTCONDITION', uppercase(trim(temp.lines[i]))) then
             begin
             temp_step:=i;
             while not(AnsiStartsStr('EXPECTED_RESULT',uppercase(trim(temp.lines[temp_step+1])))) or (temp_step < temp.lines.count-1) do
                      begin
                      temp_step:=temp_step+1;
                           if trim(temp.lines[temp_step])<>'' then
                      post_list.Items.Add(temp.lines[temp_step]);
                      end;
             end;
           if AnsiStartsStr('EXPECTED_RESULT', uppercase(trim(temp.lines[i]))) then
             begin
             temp_step:=i;
                  while (temp_step < temp.lines.count-1) do
                      begin
                      temp_step:=temp_step+1;
                      if trim(temp.lines[temp_step])<>'' then
                      exp_list.Items.Add(temp.lines[temp_step]);
                      end;
             end;
      end;
  //menuitem4.Checked:=false;
  menuitem30.click;
  end;
end;

procedure edit_existing_element(listbox: TListbox; value: string);
var
  //value : string = '';
  current_step: integer = 0;
begin
if listbox.selcount = 1 then
begin
//value := listbox.items[listbox.itemindex];
form2.ve_accept.Caption:='Edit';
form2.Show;
form2.ve_line.text:=osmae_src.get_step_pos(listbox.items[listbox.itemindex],line_sep_marker);
form2.ve_value.Text:=trim(utf8copy(value,utf8pos(line_sep_marker,value)+1,utf8length(value)));
//current_step:=strtoint(get_step_pos(listbox.Items[listbox.items.Count],line_sep_marker));
//form2.ve_line.text:=inttostr((current_step+1)*10);
form2.ve_line.SetFocus;
editor.Change:=true;
form1.menuitem30.click;
end;
end;

procedure adjust_size;
begin
with form1 do
  begin
	stringgrid1.ColWidths[0]:=100;
	stringgrid1.ColWidths[1]:=70;
	stringgrid1.ColWidths[2]:=70;
	stringgrid1.ColWidths[3]:=70;
	stringgrid1.ColWidths[4]:=panel2.width - 320;
  end;
end;

procedure TForm1.MenuItem2Click(Sender: TObject);
begin
  application.Terminate;
end;

procedure TForm1.MenuItem3Click(Sender: TObject);
begin
case PageControl2.TabIndex of
0: duplicate_item(pre_list,pre_list.itemindex);
1: duplicate_item(act_list,act_list.itemindex);
2: duplicate_item(post_list,post_list.itemindex);
3: duplicate_item(exp_list,exp_list.itemindex);
end;
end;

procedure TForm1.MenuItem4Click(Sender: TObject);
begin
    case PageControl2.TabIndex of
  0: duplicate_item(pre_list, pre_list.items.count);
  1: duplicate_item(act_list, act_list.items.count);
  2: duplicate_item(post_list, post_list.items.count);
  3: duplicate_item(exp_list, exp_list.items.count);
  end;
end;

procedure TForm1.operations_groupsChange(Sender: TObject);
var group_num, op_num, expr_num, i, j, k: integer;
  op_name, op_id: string;
begin
operations.Clear;
expressions.Clear;
if operations_groups.text<>'' then
begin
op_num := strtoint(String(EvaluateXPathExpression('count(//group[@name="'+operations_groups.text+'"]//id)', Xml.DocumentElement).AsText));
             //update expressions
             for j:=0 to op_num-1 do
                 begin
                      op_id := String(EvaluateXPathExpression('//group[@name="'+operations_groups.text+'"]//id['+inttostr(j+1)+']', Xml.DocumentElement).AsText);
                      //op_name:=String(EvaluateXPathExpression('//function/id[contains(text(),"'+op_id+'")]/../@name', Xml.DocumentElement).AsText);
                      op_name:=String(EvaluateXPathExpression('//function/id[text()="'+op_id+'"]/../@name', Xml.DocumentElement).AsText);
                      if operations.Items.IndexOf(op_name)<0 then operations.items.add(op_name);
                      //expr_num := strtoint(String(EvaluateXPathExpression('count(//group['+inttostr(i+1)+']/function['+inttostr(j+1)+']//expression)', Xml.DocumentElement).AsText));
                      //for k:=0 to expr_num-1 do
                      //if expressions.Items.IndexOf(op_name)<0 then expressions.items.add(EvaluateXPathExpression('//function['+inttostr(j+1)+']//expression['+inttostr(k+1)+']', Xml.DocumentElement).AsText);
                 end;
        operations.Sorted:=true;
end
else
begin
//count operations
op_num := strtoint(String(EvaluateXPathExpression('count(//function)', Xml.DocumentElement).AsText));
//load operations
for j:=0 to op_num-1 do
     begin
          op_name:=String(EvaluateXPathExpression('//function['+inttostr(j+1)+']/@name', Xml.DocumentElement).AsText);
          operations.items.add(op_name);
          //count expressions
          expr_num := strtoint(String(EvaluateXPathExpression('count(//function['+inttostr(j+1)+']//expression)', Xml.DocumentElement).AsText));
          //load exoressions
                   for k:=0 to expr_num-1 do
                            expressions.items.add(EvaluateXPathExpression('//function['+inttostr(j+1)+']//expression['+inttostr(k+1)+']', Xml.DocumentElement).AsText);
     end;
expressions.Sorted:=true;
operations.Sorted:=true;
end;
end;


procedure TForm1.post_listDblClick(Sender: TObject);
var value : string ='';
begin
  if post_list.SelCount=1 then begin
  value:=post_list.Items[post_list.ItemIndex];
  if inputquery('Edit value','Please edit value',value) then
  if value <>'' then
  post_list.Items[post_list.ItemIndex]:=value
  else
  showmessage('Cannot insert empty message');
  menuitem30.Click;
  end;
end;

procedure TForm1.post_listDragDrop(Sender, Source: TObject; X, Y: Integer);
  var dropindex:integer;
  begin
  dropindex:=post_list.itemAtPos(point(x,y),false); {this is where to drop it}
  {The "move" call below  will shift items up or down as necessary from
  dropindex to the slot evacuated by the dragged object to make room for the
  dragged index at the dropindex location}
  if dropindex>=0 then
  begin
    post_list.items.move(draggedindex, dropindex); {move the dragged item}
    post_list.itemindex:=dropindex;  {keep it as the selected item}
  end;
  end;

procedure TForm1.post_listDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
 Accept:=True;
end;

procedure TForm1.post_listKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
      if Key = 46 then
  MenuItem5.Click;
end;

procedure TForm1.post_listSelectionChange(Sender: TObject; User: boolean);
begin
  if post_list.SelCount=1 then
    post_list.Hint:=post_list.Items[post_list.ItemIndex];
end;

procedure TForm1.post_listStartDrag(Sender: TObject; var DragObject: TDragObject
  );
begin
  draggedindex:=post_List.itemindex;
end;

procedure TForm1.pre_listContextPopup(Sender: TObject; MousePos: TPoint;
  var Handled: Boolean);
begin
//if pre_list.SelCount=1 then
  popupmenu1.PopUp;
end;

procedure TForm1.pre_listDblClick(Sender: TObject);
  var value : string ='';
  begin
    if pre_list.SelCount=1 then begin
    value:=pre_list.Items[pre_list.ItemIndex];
    if inputquery('Edit value','Please edit value',value) then
    if value <>'' then
    pre_list.Items[pre_list.ItemIndex]:=value
    else
    showmessage('Cannot insert empty message');
    menuitem30.Click;
    end;
  end;

procedure TForm1.pre_listDragDrop(Sender, Source: TObject; X, Y: Integer);
  var dropindex:integer;
  begin
    dropindex:=pre_list.itemAtPos(point(x,y),false); {this is where to drop it}
    {The "move" call below  will shift items up or down as necessary from
    dropindex to the slot evacuated by the dragged object to make room for the
    dragged index at the dropindex location}
    if dropindex>=0 then
    begin
      pre_list.items.move(draggedindex, dropindex); {move the dragged item}
      pre_list.itemindex:=dropindex;  {keep it as the selected item}
    end;
  end;

procedure TForm1.pre_listDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  Accept:=True;
end;

procedure TForm1.pre_listKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 46 then
  MenuItem5.Click;
end;

procedure TForm1.pre_listSelectionChange(Sender: TObject; User: boolean);
begin
   if pre_list.SelCount=1 then
  pre_list.Hint:=pre_list.Items[pre_list.ItemIndex];
end;

procedure TForm1.pre_listStartDrag(Sender: TObject; var DragObject: TDragObject
  );
begin
    draggedindex:=pre_List.itemindex;
end;

procedure TForm1.Splitter2Moved(Sender: TObject);
begin
adjust_size;
end;

procedure TForm1.post_listContextPopup(Sender: TObject; MousePos: TPoint;
  var Handled: Boolean);
begin
      //if post_list.SelCount=1 then
    popupmenu1.PopUp;
end;

procedure TForm1.Edit1Change(Sender: TObject);
var i: integer;
begin
listbox8.clear;
  for i:=0 to Flistmemory.Count-1 do
  if ((strSimilarity(trim(uppercase(FlistMemory[i])),trim(uppercase(edit1.text))))<3) or (strContains(trim(uppercase(FlistMemory[i])),trim(uppercase(edit1.text)))) then
     listbox8.Items.add(FlistMemory[i]);
end;

procedure TForm1.act_listContextPopup(Sender: TObject; MousePos: TPoint;
  var Handled: Boolean);
begin
    //if act_list.SelCount=1 then
    popupmenu1.PopUp;
end;

procedure TForm1.act_listDblClick(Sender: TObject);
var value : string ='';
begin
begin
  if act_list.SelCount=1 then begin
  value:=act_list.Items[act_list.ItemIndex];
  if inputquery('Edit value','Please edit value',value) then
  if value <>'' then
  act_list.Items[act_list.ItemIndex]:=value
  else
  showmessage('Cannot insert empty message');
  menuitem30.Click;
  end;
end;
end;

procedure TForm1.act_listDragDrop(Sender, Source: TObject; X, Y: Integer);
var dropindex:integer;
begin
  dropindex:=act_list.itemAtPos(point(x,y),false); {this is where to drop it}
  {The "move" call below  will shift items up or down as necessary from
  dropindex to the slot evacuated by the dragged object to make room for the
  dragged index at the dropindex location}
  if dropindex>=0 then
  begin
    act_list.items.move(draggedindex, dropindex); {move the dragged item}
    act_list.itemindex:=dropindex;  {keep it as the selected item}
  end;
end;

procedure TForm1.act_listDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  Accept:=True;
end;

procedure TForm1.act_listKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
      if Key = 46 then
  MenuItem5.Click;
end;

procedure TForm1.act_listSelectionChange(Sender: TObject; User: boolean);
begin
    if act_list.SelCount=1 then
    act_list.Hint:=act_list.Items[act_list.ItemIndex];
end;

procedure TForm1.act_listStartDrag(Sender: TObject; var DragObject: TDragObject
  );
begin
  draggedindex:=act_List.itemindex;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  load_xml(filelistbox1.FileName);
  pagecontrol1.PageIndex:=1;
  statusbar1.Panels[4].Text:='File: '+filelistbox1.FileName;
  showmessage(filelistbox1.FileName+ ' successful loaded!');
end;

procedure TForm1.Button2Click(Sender: TObject);
var UserString: string = '';
  OldName: string;
begin
  OldName:=Filelistbox1.Items[filelistbox1.itemindex];
  if filelistbox1.SelCount>0 then
  UserString:=InputBox('Renaming','Please input new name',OldName);
  if UserString<>OldName then
  begin
  if UserString<>'' then
  begin
  if RenameFile((filelistbox1.Directory+'\'+Filelistbox1.Items[filelistbox1.itemindex]),(filelistbox1.Directory+'\'+UserString))
  then
  begin
  filelistbox1.UpdateFileList;
  htmlviewer1.Visible:=false;
  filelistbox1.Selected[filelistbox1.Items.IndexOf(UserString)]:=true;
  synedit1.Clear;
  showmessage('Renamed');
  end
  else
  showmessage('Could not rename');
  end;
  end
  else
  showmessage('Renaming canceled, same name');
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
if filelistbox1.SelCount>0 then
begin
if MessageDlg('Question', 'Do you wish to delete the selected file?', mtConfirmation,
   [mbYes, mbNo, mbIgnore],0) = mrYes
  then
  if deleteFile((filelistbox1.Directory+'\'+Filelistbox1.Items[filelistbox1.itemindex])){ Execute rest of Program }
  then
  begin
  filelistbox1.UpdateFileList;
  htmlviewer1.Visible:=false;
  synedit1.Clear;
  showmessage('Deleted');
  end
  else
  showmessage('Could not delete');
  end;
end;

procedure TForm1.Button4Click(Sender: TObject);
var value : string = '';
  current_step: integer = 0;
begin
if expressions.selcount = 1 then
begin
if pagecontrol2.tabindex = 4 then
   begin
   showmessage('To add an expression, first select the block (Adding to Description is not possible)');
   exit;
   end
else
begin
//case pagecontrol2.tabindex of
//0: current_step:=strtoint(get_step_pos(pre_list.Items[pre_list.items.Count],line_sep_marker));
//1: current_step:=strtoint(get_step_pos(pre_list.Items[act_list.items.Count],line_sep_marker));
//2: current_step:=strtoint(get_step_pos(pre_list.Items[post_list.items.Count],line_sep_marker));
//3: current_step:=strtoint(get_step_pos(pre_list.Items[exp_list.items.Count],line_sep_marker));
//end;
end;
value := expressions.items[expressions.itemindex];
begin
form2.Show;
form2.ve_accept.Caption:='Add';
editor.Change:=false;
form2.ve_value.Text:=value;
//form2.ve_line.text:=inttostr(current_step+10);
form2.ve_line.SetFocus;
end;
end
else
begin
     if pagecontrol2.tabindex = 4 then
        begin
        showmessage('To add an expression, first select the block (Adding to Description is not possible)');
        exit;
        end
     else
     begin
     //case pagecontrol2.tabindex of
     //0: current_step:=strtoint(get_step_pos(pre_list.Items[pre_list.items.Count],line_sep_marker));
     //1: current_step:=strtoint(get_step_pos(pre_list.Items[act_list.items.Count],line_sep_marker));
     //2: current_step:=strtoint(get_step_pos(pre_list.Items[post_list.items.Count],line_sep_marker));
     //3: current_step:=strtoint(get_step_pos(pre_list.Items[exp_list.items.Count],line_sep_marker));
     //end;
     end;
     value := '';
     begin
     form2.Show;
     form2.ve_accept.Caption:='Add';
     editor.Change:=false;
     form2.ve_value.Text:=value;
     //form2.ve_line.text:=inttostr((current_step+1)*10);
     form2.ve_line.SetFocus;
     end;
end;
end;

procedure TForm1.Button5Click(Sender: TObject);
var g_path: string;
    MemoryFiles: TStringList;
    i,j: integer;
    Xml_Temp: TXMLDocument;
    step_count: integer;
    step_temp: string;
begin
  g_path := ExtractFilePath(Application.ExeName)+'projects';
  if directoryexists(g_path) then begin
  //showmessage(g_path);
  MemoryFiles := TStringList.Create;
  try
    FindAllFiles(MemoryFiles, g_path, '*.tsfx', true); //find e.g. all pascal sourcefiles
    //ShowMessage(Format('Found %d files', [MemoryFiles.Count]));
    for i := 0 to MemoryFiles.Count-1 do
    begin
    ReadXMLFile(Xml_Temp, MemoryFiles[i]);
    step_count:=round(EvaluateXPathExpression('count(//step)', Xml_Temp.DocumentElement).AsNumber);
  for j:=1 to step_count do
  begin
  step_temp:=String(EvaluateXPathExpression('(//step)['+inttostr(j)+']', Xml_Temp.DocumentElement).AsText);
  if FlistMemory.IndexOf(step_temp)<0 then
    FlistMemory.Add(step_temp);
    end;
    end;
  finally
    MemoryFiles.Free;
    FlistMemory.Sorted:=true;
  end;
  end;
end;

procedure TForm1.Button6Click(Sender: TObject);
begin
end;

procedure TForm1.descEditingDone(Sender: TObject);
begin
  menuitem30.Click;
end;

procedure TForm1.Edit1KeyPress(Sender: TObject; var Key: char);
begin
  begin
  if ord(Key)=27 then
  begin
  expressions_filter.Text:='';
  unselect_all(expressions);
  end;
  if ord(Key)=13 then
begin
case PageControl2.TabIndex of
  0: pre_list.Items.Add(edit1.Text);
  1: act_list.Items.Add(edit1.Text);
  2: post_list.Items.Add(edit1.Text);
  3: exp_list.Items.Add(edit1.Text);
  4: showmessage('Choose appropriate block. Adding to Overview is not supported');
end;
if FlistMemory.IndexOf(edit1.Text)=-1 then
   FlistMemory.Add(edit1.Text);
FlistMemory.Sorted:=true;
listbox4.items:=FlistMemory;
end;
end;

end;

procedure TForm1.EditButton1ButtonClick(Sender: TObject);
begin
  editbutton1.Text:='';
  filelistbox1.mask:='*.tsfx';
end;

procedure TForm1.EditButton1Change(Sender: TObject);
begin
  filelistbox1.Mask:='*'+editbutton1.Text+'*.tsfx';
end;

procedure TForm1.expressionsDblClick(Sender: TObject);
begin
  button4.Click;
end;

procedure TForm1.expressionsKeyPress(Sender: TObject; var Key: char);
begin
      if ord(Key)=13 then
  button4.Click;
end;

procedure TForm1.expressionsKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var cur_pos: integer;
begin
    if Key = 46 then
    begin
    if expressions.SelCount=1 then
    begin
    cur_pos:=expressions.ItemIndex;
    FlistExpr.Delete(FlistExpr.IndexOf(expressions.Items[expressions.ItemIndex]));
    expressions.Items.Delete(expressions.ItemIndex);
    if expressions.Items.Count<>0 then
    expressions.Selected[cur_pos]:=true;
    end;
    end;
end;

function reg_replace(input_string: string; find_string: string; replace_string: string): string;
var r: TRegExpr;
    sResult: string ='';
begin
         r:= TRegExpr.Create;
    try
        r.Expression := find_string;
        sResult:= r.Replace(input_string,replace_string,true);
        sResult:=trim(DelSpace1(sResult));
    finally
        r.Free;
    end;
    result:=sResult;
end;

procedure TForm1.expressionsSelectionChange(Sender: TObject; User: boolean);
var i, j, k: integer;
  start_pos : integer = 0;
  funct_num : integer = 0;
  param_num : integer = 0;
  selected_expr, selected_op, selected_group: string;
  function_name : string;
  r: TRegExpr;
  sResult : string = '';
  group_option : string;
begin
edit1.text:='';
edit2.text:='';
edit3.text:='';
edit4.text:='';
stringgrid1.RowCount:=1;
combobox1.Clear;
combobox1.Text:='';

    if expressions.SelCount = 1 then
         edit1.text:=trim(expressions.items[expressions.itemindex]);
         r:= TRegExpr.Create;
    try
        r.Expression := '\$[0-9]+';
        sResult:= r.Replace(Edit1.Text,' ',true);
        Edit1.Text:=trim(DelSpace1(sResult));
    finally
        r.Free;
    end;

         //get selected expression
         selected_expr:=expressions.items[expressions.itemindex];
         //get the name of the selected operation
         if operations.SelCount<>0 then selected_op:=operations.Items[operations.itemindex] else selected_op:='';
         //get the name of the group, if one is chosen
         selected_group:=operations_groups.text;
         if operations_groups.text<>'' then
            group_option:='//group[@name='+selected_group+']'
         else
            group_option:='';

         //edit2.text := EvaluateXPathExpression('//expression[contains(text(), "'+selected_expr+'")]//..//../@name', Xml.DocumentElement).AsText;
         edit2.text := EvaluateXPathExpression('//expression[text()="'+selected_expr+'"]//..//../@name', Xml.DocumentElement).AsText;
         //edit3.text := EvaluateXPathExpression('//expression[contains(text(), "'+selected_expr+'")]//..//../id', Xml.DocumentElement).AsText;
         edit3.text := EvaluateXPathExpression('//expression[text()="'+selected_expr+'"]//..//../id', Xml.DocumentElement).AsText;
         //edit4.text := EvaluateXPathExpression('//expression[contains(text(), "'+selected_expr+'")]//..//../f_description', Xml.DocumentElement).AsText;
         edit4.text := EvaluateXPathExpression('//expression[text()="'+selected_expr+'"]//..//../f_description', Xml.DocumentElement).AsText;
         //param_num:=strtoint(EvaluateXPathExpression('count(//expression[contains(text(), "'+selected_expr+'")]//..//..//parameter)', Xml.DocumentElement).AsText);
         param_num:=strtoint(EvaluateXPathExpression('count(//expression[text()="'+selected_expr+'"]//..//..//parameter)', Xml.DocumentElement).AsText);
         label3.caption:='Parameters: ' + inttostr(param_num);
         if param_num = 0 then
         combobox1.Text:='No parameters'
         else
         begin
         for k:=0 to param_num-1 do
         begin
             stringgrid1.RowCount:=stringgrid1.RowCount+1;
             //stringgrid1.Cells[0,k+1]:=EvaluateXPathExpression('//expression[contains(text(), "'+selected_expr+'")]//..//../parameter['+inttostr(k+1)+']/placeholder', Xml.DocumentElement).AsText;
             //stringgrid1.Cells[1,k+1]:=EvaluateXPathExpression('//expression[contains(text(), "'+selected_expr+'")]//..//../parameter['+inttostr(k+1)+']/defaultValue', Xml.DocumentElement).AsText;
             //stringgrid1.Cells[2,k+1]:=EvaluateXPathExpression('//expression[contains(text(), "'+selected_expr+'")]//..//../parameter['+inttostr(k+1)+']/unit', Xml.DocumentElement).AsText;
             //stringgrid1.Cells[3,k+1]:=EvaluateXPathExpression('//expression[contains(text(), "'+selected_expr+'")]//..//../parameter['+inttostr(k+1)+']/type', Xml.DocumentElement).AsText;
             //stringgrid1.Cells[4,k+1]:=EvaluateXPathExpression('//expression[contains(text(), "'+selected_expr+'")]//..//../parameter['+inttostr(k+1)+']/p_description', Xml.DocumentElement).AsText;
             stringgrid1.Cells[0,k+1]:=EvaluateXPathExpression('//expression[text()="'+selected_expr+'"]//..//../parameter['+inttostr(k+1)+']/placeholder', Xml.DocumentElement).AsText;
             stringgrid1.Cells[1,k+1]:=EvaluateXPathExpression('//expression[text()="'+selected_expr+'"]//..//../parameter['+inttostr(k+1)+']/defaultValue', Xml.DocumentElement).AsText;
             stringgrid1.Cells[2,k+1]:=EvaluateXPathExpression('//expression[text()="'+selected_expr+'"]//..//../parameter['+inttostr(k+1)+']/unit', Xml.DocumentElement).AsText;
             stringgrid1.Cells[3,k+1]:=EvaluateXPathExpression('//expression[text()="'+selected_expr+'"]//..//../parameter['+inttostr(k+1)+']/type', Xml.DocumentElement).AsText;
             stringgrid1.Cells[4,k+1]:=EvaluateXPathExpression('//expression[text()="'+selected_expr+'"]//..//../parameter['+inttostr(k+1)+']/p_description', Xml.DocumentElement).AsText;
         end;
         end;
    end;

procedure TForm1.expressionsStartDrag(Sender: TObject;
  var DragObject: TDragObject);
begin
  draggedindex:=expressions.itemindex;
end;

procedure TForm1.expressions_filterButtonClick(Sender: TObject);
begin
   expressions_filter.text:='';
end;

procedure TForm1.expressions_filterChange(Sender: TObject);
var
  I: Integer;
  S: String;
begin
  expressions.Items.BeginUpdate;
  try
    begin
    expressions.Clear;
    //unselect_all(operations);
    //if expressions_filter.GetTextLen > 0 then
    if trim(expressions_filter.Text) <> '' then
    begin
      S := expressions_filter.Text;
      for I := 0 to FListExpr.Count - 1 do
      begin
        if pos(lowercase(S), lowercase(FListExpr[I]))>0 then
          expressions.Items.Add(FListExpr[I]);
      end;
    end
 else
 begin
     expressions.Items.BeginUpdate;
     expressions.Clear;
     for I := 0 to FListExpr.Count - 1 do
       expressions.Items.Add(FListExpr[I]);
     expressions.Items.EndUpdate;
 end;
    end;
  finally
    expressions.Sorted:=true;
    statusbar2.SimpleText:=inttostr(operations.Items.Count) + ' operation(s)';
    statusbar3.SimpleText:=inttostr(expressions.Items.Count) + ' expression(s)';
    expressions.Items.EndUpdate;
  end;
end;

procedure TForm1.expressions_filterKeyPress(Sender: TObject; var Key: char);
begin
  if ord(Key)=27 then
  begin
  expressions_filter.Text:='';
  unselect_all(expressions);
  end;
  if ord(Key)=13 then
begin
case PageControl2.TabIndex of
  0: pre_list.Items.Add(expressions_filter.Text);
  1: act_list.Items.Add(expressions_filter.Text);
  2: post_list.Items.Add(expressions_filter.Text);
  3: exp_list.Items.Add(expressions_filter.Text);
  4: showmessage('Choose appropriate block. Adding to Description is not supported');
end;

if FlistExpr.IndexOf(expressions_filter.text)=-1 then
   FListExpr.Add(expressions_filter.text);
   expressions_filter.OnButtonClick(expressions_filter);
end;
end;

procedure TForm1.MenuItem11Click(Sender: TObject);
begin
  if opendialog2.Execute and fileexists(opendialog2.FileName)then
  begin
  read_dict(opendialog2.FileName);
  //showmessage('OperationMapping loaded!');
  statusbar1.Panels[0].Text:='Dict: '+extractfilename(opendialog2.FileName);
  end;
end;

procedure TForm1.MenuItem13Click(Sender: TObject);
begin

end;

procedure TForm1.MenuItem30Click(Sender: TObject);
var i : integer;
  line_count : integer = 0;
  line_count_offset : integer = 0;
  SynMarkup: TSynEditMarkupHighlightAllCaret;
  r: TRegExpr;
begin
  //if MenuItem4.Checked then line_count_offset := 10
  //else line_count_offset := 0;

  line_count_offset := 10;
  overview.Clear;

  r:= TRegExpr.Create;
  r.Expression := '^[0-9]+'+line_sep_marker;

  overview.Lines.Add('Description');

  if trim(desc.text)<>'' then
  begin
    overview.lines.add(trimright(desc.text));
    overview.Lines.Add('');
  end;

  overview.Lines.Add('');

  overview.Lines.Add('PRECONDITION');
  for i:= 0 to pre_list.Items.Count-1 do
  begin
  if strSlice(pre_list.items[i],1,1)='#' then
    begin
    overview.lines.add(pre_list.items[i]);
    Continue;
    end;
  if not r.Exec(pre_list.items[i]) then
  begin
       line_count := line_count + line_count_offset;
       overview.lines.add(generate_line(line_count) + pre_list.items[i])
  end
  else
  begin
      overview.lines.add(pre_list.items[i]);
      line_count:=strtoint(get_step_pos(pre_list.items[i],line_sep_marker));
      //showmessage(inttostr(line_count));
  end;
  end;

  overview.Lines.Add('');
  overview.Lines.Add('ACTION');
  for i:= 0 to act_list.Items.Count-1 do
  begin
    if strSlice(act_list.items[i],1,1)='#' then
    begin
    overview.lines.add(act_list.items[i]);
    Continue;
    end;

  if not r.Exec(act_list.items[i]) then
  begin
       line_count := line_count + line_count_offset;
       overview.lines.add(generate_line(line_count) + act_list.items[i])
  end
  else
  begin
      overview.lines.add(act_list.items[i]);
      line_count:=strtoint(get_step_pos(act_list.items[i],line_sep_marker));
  end;
  end;
  overview.Lines.Add('');
  overview.Lines.Add('POSTCONDITION');
  for i:= 0 to post_list.Items.Count-1 do
  begin
    if strSlice(post_list.items[i],1,1)='#' then
    begin
    overview.lines.add(post_list.items[i]);
    Continue;
    end;

  if not r.Exec(post_list.items[i]) then
  begin
       line_count := line_count + line_count_offset;
       overview.lines.add(generate_line(line_count) + post_list.items[i])
  end
  else
  begin
      overview.lines.add(post_list.items[i]);
      line_count:=strtoint(get_step_pos(post_list.items[i],line_sep_marker));
  end;
  end;
  overview.Lines.Add('');
  overview.Lines.Add('EXPECTED_RESULT');
  for i:= 0 to exp_list.Items.Count-1 do
  begin
    if strSlice(exp_list.items[i],1,1)='#' then
    begin
    overview.lines.add(exp_list.items[i]);
    Continue;
    end;

  if not r.Exec(exp_list.items[i]) then
  begin
       line_count := line_count + line_count_offset;
       overview.lines.add(generate_line(line_count) + exp_list.items[i])
  end
  else
  begin
      overview.lines.add(exp_list.items[i]);
      line_count:=strtoint(get_step_pos(exp_list.items[i],line_sep_marker));
  end;
  end;
  overview.Text:=overview.Text;
  save_xml(ExtractFilePath(Application.ExeName)+'projects\'+'.autosave\temp.tsfx');
  r.Free;
end;

procedure TForm1.load_project(project: string);
var sub_proj: string;
begin
with form1 do
  begin
  ReadXMLFile(Xml_Project, project);
  osmp_author:=EvaluateXPathExpression('/osmp/author', Xml_Project.DocumentElement).AsText;
  osmp_dict:= EvaluateXPathExpression('/osmp/dict', Xml_Project.DocumentElement).AsText;
  osmp_project:= EvaluateXPathExpression('/osmp/project', Xml_Project.DocumentElement).AsText;
  line_sep_marker:= EvaluateXPathExpression('/osmp/separator', Xml_Project.DocumentElement).AsText;
  sub_proj:=EvaluateXPathExpression('/osmp/sub-project', Xml_Project.DocumentElement).AsText;

  filelistbox2.Directory:=osmp_project;
  filelistbox2.UpdateFileList;
  filelistbox2.Selected[filelistbox2.Items.IndexOf('['+sub_proj+']')];
  filelistbox1.Directory:=osmp_project+'\'+sub_proj;
  filelistbox1.UpdateFileList;
  read_dict(osmp_dict);
  statusbar1.Panels[0].Text:='Dict: '+extractfilename(osmp_dict);
  statusbar1.Panels[1].Text:='Separator: '+line_sep_marker;
  labelededit4.Text:=line_sep_marker;
  statusbar1.Panels[2].Text:='Project: '+sub_proj;
  end;
end;

procedure TForm1.MenuItem19Click(Sender: TObject);
var sub_proj: string;
begin
  if opendialog3.Execute then
  begin
     load_project(opendialog3.FileName);
     showmessage('Loading project complete');
  end;
end;

procedure TForm1.MenuItem22Click(Sender: TObject);
begin
case PageControl2.TabIndex of
  //0: duplicate_item(pre_list, pre_list.items.count);
  1: move_item(act_list, pre_list);
  2: move_item(post_list, pre_list);
  3: move_item(exp_list, pre_list);
  end;
menuitem30.click;
end;

procedure TForm1.MenuItem23Click(Sender: TObject);
begin
  case PageControl2.TabIndex of
  0: move_item(pre_list, act_list);
  //1: duplicate_item(act_list, act_list.items.count);
  2: move_item(post_list, act_list);
  3: move_item(exp_list, act_list);
  end;
  menuitem30.click;
end;

procedure TForm1.MenuItem24Click(Sender: TObject);
begin
  case PageControl2.TabIndex of
  0: move_item(pre_list, post_list);
  1: move_item(act_list, post_list);
  //2: duplicate_item(post_list, post_list.items.count);
  3: move_item(exp_list, post_list);
  end;
  menuitem30.click;
end;

procedure TForm1.MenuItem25Click(Sender: TObject);
begin
    case PageControl2.TabIndex of
  0: move_item(pre_list, exp_list);
  1: move_item(act_list, exp_list);
  2: move_item(post_list, exp_list);
  //3: duplicate_item(exp_list, post_list.items.count);
  end;
  menuitem30.click;
end;

procedure TForm1.MenuItem28Click(Sender: TObject);
var cur_pos: integer;
begin
  case PageControl2.TabIndex of
  0: begin
     pre_list.Clear;
     end;
  1: begin
     act_list.Clear;
     end;
  2: begin
     post_list.Clear;
     end;
  3: begin
     exp_list.Clear;
  end;
end;
end;

procedure TForm1.MenuItem29Click(Sender: TObject);
begin
end;

procedure TForm1.MenuItem5Click(Sender: TObject);
var cur_pos: integer;
begin
  case PageControl2.TabIndex of
  0: begin
  if pre_list.SelCount<>0 then
     cur_pos:=pre_list.itemindex;
     pre_list.Items.Delete(cur_pos);
     if (pre_list.Items.Count>0) then
       if cur_pos<>0 then pre_list.Selected[cur_pos-1]:=true
       else
       pre_list.Selected[cur_pos]:=true
     end;
  1: begin
  if act_list.SelCount<>0 then
    cur_pos:=act_list.itemindex;
    act_list.Items.Delete(cur_pos);
  if (act_list.Items.Count>0) then
    if cur_pos<>0 then act_list.Selected[cur_pos-1]:=true
    else
    act_list.Selected[cur_pos]:=true
     end;
  2: begin
  if post_list.SelCount<>0 then
    cur_pos:=post_list.itemindex;
    post_list.Items.Delete(cur_pos);
  if (post_list.Items.Count>0) then
    if cur_pos<>0 then post_list.Selected[cur_pos-1]:=true
    else
    post_list.Selected[cur_pos]:=true
     end;
  3: begin
  if exp_list.SelCount<>0 then
    cur_pos:=exp_list.itemindex;
    exp_list.Items.Delete(cur_pos);
  if (exp_list.Items.Count>0) then
    if cur_pos<>0 then exp_list.Selected[cur_pos-1]:=true
    else
    exp_list.Selected[cur_pos]:=true
  end;
end;
end;

procedure TForm1.MenuItem6Click(Sender: TObject);
begin
  case PageControl2.TabIndex of
 0: edit_existing_element(pre_list, pre_list.items[pre_list.itemindex]);
 1: edit_existing_element(act_list, act_list.items[act_list.itemindex]);
 2: edit_existing_element(post_list, post_list.items[post_list.itemindex]);
 3: edit_existing_element(exp_list, exp_list.items[exp_list.itemindex]);
 end;
end;

procedure TForm1.operationsSelectionChange(Sender: TObject; User: boolean);
var i: integer;
  start_pos, end_pos: integer;
  op_name, group_name: string;
  selected_op : string;
  expressions_number : integer;
  selected_group: string;
  temp_string: string;
  group_option: string;
begin
edit1.text:='';
edit2.text:='';
edit3.text:='';
edit4.text:='';
combobox1.Clear;
combobox1.Text:='';
stringgrid1.RowCount:=1;
//if an operation is selected
if operations.selcount=1 then
   begin
        //clear the expression list
        expressions.clear;
        //get the name of the selected operation
        selected_op:=operations.Items[operations.itemindex];
        //get the name of the group, if one is chosen
        selected_group:=operations_groups.text;
        //if trim(operations_groups.text)<>'' then
        //   group_option:='//group[@name="'+selected_group+'"]'
        //else
        group_option:='';
        //get the expressions from the xml; if no group is selected, group_option is empty and won't be regarded
        expressions_number := strtoint(EvaluateXPathExpression('count('+group_option+'//function[@name="'+selected_op+'"]//expression)', Xml.DocumentElement).AsText);
        //if there are any expressions found
        if expressions_number>0 then
           begin
                //loop through the number of expressions
                for i:=1 to expressions_number do
                    //ignore
                       begin
                            //get i-th expression
                            temp_string :=trim(EvaluateXPathExpression(group_option+'//function[@name="'+selected_op+'"]//expression['+inttostr(i)+']', Xml.DocumentElement).AsText);
                            //add it to list of expressions
                            if temp_string<>'' then
                               expressions.Items.Add(temp_string)
                       end
           end;
        //update the count of the operations and expressions
        statusbar2.SimpleText:=inttostr(operations.Items.Count) + ' operation(s)';
        statusbar3.SimpleText:=inttostr(expressions.Items.Count) + ' expression(s)';
  end;
end;


procedure TForm1.exp_listContextPopup(Sender: TObject; MousePos: TPoint;
  var Handled: Boolean);
begin
  //if exp_list.SelCount=1 then
    popupmenu1.PopUp;
end;

procedure TForm1.exp_listDblClick(Sender: TObject);
var value : string ='';
begin
  if exp_list.SelCount=1 then begin
  value:=exp_list.Items[exp_list.ItemIndex];
  if inputquery('Edit value','Please edit value',value) then
  if value <>'' then
  exp_list.Items[exp_list.ItemIndex]:=value
  else
  showmessage('Cannot insert empty message');
  menuitem30.Click;
  end;
end;

procedure TForm1.exp_listDragDrop(Sender, Source: TObject; X, Y: Integer);
var dropindex:integer;
begin
  dropindex:=exp_list.itemAtPos(point(x,y),false); {this is where to drop it}
  {The "move" call below  will shift items up or down as necessary from
  dropindex to the slot evacuated by the dragged object to make room for the
  dragged index at the dropindex location}
  if dropindex>=0 then
  begin
    exp_list.items.move(draggedindex, dropindex); {move the dragged item}
    exp_list.itemindex:=dropindex;  {keep it as the selected item}
  end;
end;

procedure TForm1.exp_listDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
    Accept:=True;
end;

procedure TForm1.exp_listKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
      if Key = 46 then
  MenuItem5.Click;
end;

procedure TForm1.exp_listSelectionChange(Sender: TObject; User: boolean);
begin
  if exp_list.SelCount=1 then
  exp_list.Hint:=exp_list.Items[exp_list.ItemIndex];
end;

procedure TForm1.exp_listStartDrag(Sender: TObject; var DragObject: TDragObject
  );
begin
  draggedindex:=exp_List.itemindex;
end;

procedure TForm1.FileListBox1Change(Sender: TObject);
begin
  if filelistbox1.SelCount=1 then
if fileexists(filelistbox1.FileName) then
  synedit1.Lines.LoadFromFile(filelistbox1.FileName);
end;

procedure DeleteFiles(APath, AFileSpec: string);
var
  lSearchRec:TSearchRec;
  lPath:string;
begin
  lPath := IncludeTrailingPathDelimiter(APath);

  if FindFirst(lPath+AFileSpec,faAnyFile,lSearchRec) = 0 then
  begin
    try
      repeat
        SysUtils.DeleteFile(lPath+lSearchRec.Name);
      until SysUtils.FindNext(lSearchRec) <> 0;
    finally
      SysUtils.FindClose(lSearchRec);  // Free resources on successful find
    end;
  end;
end;

procedure create_html(filename: string);
var AProcess: TProcess;
begin
  with form1 do
  begin
  AProcess:=TProcess.Create(nil);
  AProcess.CommandLine:='msxsl.exe "'+filename+'" "'+ExtractFilePath(Paramstr(0))+'\tsf.xsl"'+' '+'-o "'+replacestr(filename,'.tsfx','.html')+'"';
  AProcess.Options:=AProcess.Options + [poWaitOnExit, poNoConsole];
  try
    AProcess.Execute;
    //Result:=true;
  except
  on E: Exception do
  begin
     showmessage('Execution failed');
  //Result:=false;
  end;
  end;
  AProcess.Free;
  end;
end;

procedure TForm1.FileListBox1SelectionChange(Sender: TObject; User: boolean);
var xml_file, html_file: string;
begin
  if filelistbox1.SelCount>0 then
  begin
  xml_file:=filelistbox1.Directory+'\'+filelistbox1.items[filelistbox1.itemindex];
  html_file:=filelistbox1.Directory+'\'+replacestr(filelistbox1.items[filelistbox1.itemindex],'.tsfx','.html');
  create_html(xml_file);
  htmlviewer1.LoadFromFile(html_file);
  htmlviewer1.Visible:=true;
  DeleteFiles(form1.FileListBox1.Directory,'*.html');
  button1.Enabled:=true;
  button2.Enabled:=true;
  button3.Enabled:=true;
  end;
end;

procedure TForm1.FileListBox2Change(Sender: TObject);
begin
    filelistbox1.Directory:=filelistbox2.FileName;
      button1.Enabled:=false;
  button2.Enabled:=false;
  button3.Enabled:=false;
end;

procedure TForm1.FileListBox2SelectionChange(Sender: TObject; User: boolean);
begin
  if filelistbox1.SelCount=0 then begin
  button1.Enabled:=false;
  button2.Enabled:=false;
  button3.Enabled:=false;
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  pre_list.DragMode:=dmAutomatic;
  FList := TStringList.Create;
  FListExpr := TStringList.Create;
  Flistmemory:=TStringList.Create;
  editor.Sig_List:=TStringList.Create;
  if fileexists('default.xml') then read_dict('default.xml');
  button5.Click;
  filelistbox2.Directory:=ExtractFileDir(ParamStr(0))+'\projects';
  filelistbox1.Directory:=filelistbox2.Directory;
  pagecontrol1.PageIndex:=0;
  pagecontrol2.PageIndex:=0;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
      FList.Free;
  FListExpr.Free;
  editor.Sig_List.free;
end;

procedure TForm1.FormResize(Sender: TObject);
begin
  adjust_size;
end;

procedure TForm1.FormShow(Sender: TObject);
begin

end;

procedure TForm1.GroupBox5DblClick(Sender: TObject);
begin
    groupbox5.Width:=0;
  form1.Resize;
end;

procedure TForm1.LabeledEdit1Change(Sender: TObject);
begin
  statusbar1.Panels[3].Text:='Name:'+LabeledEdit1.text;
end;

procedure TForm1.LabeledEdit4Change(Sender: TObject);
begin
  line_sep_marker:=labelededit4.Text;
  statusbar1.Panels[1].Text:='Separator: '+line_sep_marker;
end;

procedure TForm1.ListBox8DblClick(Sender: TObject);
  begin
  if listbox8.SelCount<>0 then
  {begin
case PageControl2.TabIndex of
   0: pre_list.items.add(listbox8.Items[listbox8.ItemIndex]);
   1: act_list.items.add(listbox8.Items[listbox8.ItemIndex]);
   2: post_list.items.add(listbox8.Items[listbox8.ItemIndex]);
   3: exp_list.items.add(listbox8.Items[listbox8.ItemIndex]);
   4: showmessage('Choose appropriate block. Adding to Descrption is not supported');
end;
end;}

  begin
       if pagecontrol2.tabindex = 4 then
          begin
          showmessage('To add an expression, first select the block (Adding to Description is not possible)');
          exit;
          end
       else
       begin
       //case pagecontrol2.tabindex of
       //0: current_step:=strtoint(get_step_pos(pre_list.Items[pre_list.items.Count],line_sep_marker));
       //1: current_step:=strtoint(get_step_pos(pre_list.Items[act_list.items.Count],line_sep_marker));
       //2: current_step:=strtoint(get_step_pos(pre_list.Items[post_list.items.Count],line_sep_marker));
       //3: current_step:=strtoint(get_step_pos(pre_list.Items[exp_list.items.Count],line_sep_marker));
       //end;
       end;
       value := listbox8.Items[listbox8.ItemIndex];
       begin
       form2.Show;
       form2.ve_accept.Caption:='Add';
       editor.Change:=false;
       form2.ve_value.Text:=value;
       //form2.ve_line.text:=inttostr((current_step+1)*10);
       form2.ve_line.SetFocus;
       end;
  end;

end;

procedure TForm1.ListBox8ShowHint(Sender: TObject; HintInfo: PHintInfo);
begin
end;

procedure TForm1.MenuItem10Click(Sender: TObject);
begin
if (opendialog1.Execute) then
begin
    if opendialog1.FilterIndex=1 then
    load_xml(opendialog1.FileName)
  else if opendialog1.FilterIndex=2 then load_txt;
    statusbar1.Panels[4].Text:='File: '+opendialog1.FileName;
end;
end;

procedure save_txt(filename : String);
begin
with form1 do
begin
  overview.lines.SaveToFile(filename);
  //showmessage('Saving complete!');
end;
end;

procedure TForm1.MenuItem14Click(Sender: TObject);
begin
      if (savedialog1.Execute) then
      if savedialog1.FilterIndex=1 then begin save_xml(savedialog1.FileName);
      statusbar1.Panels[3].Text:='Name:'+LabeledEdit1.text;
      statusbar1.Panels[4].Text:='File:'+savedialog1.filename;
      showmessage('Saving complete!'); end
    else if savedialog1.FilterIndex=2 then begin save_txt(savedialog1.FileName);
    statusbar1.Panels[3].Text:='Name:'+LabeledEdit1.text;
    statusbar1.Panels[4].Text:='File:'+savedialog1.filename;
    showmessage('Saving complete!'); end;
      filelistbox1.UpdateFileList;
      filelistbox1.Refresh;
end;

procedure TForm1.MenuItem21Click(Sender: TObject);
begin

end;

procedure TForm1.MenuItem8Click(Sender: TObject);
begin
  clear_editor;
  overview.Clear;
  overview.Clear;
end;

procedure TForm1.operations_filterButtonClick(Sender: TObject);
begin
    operations_filter.text:='';
  unselect_all(operations);
  expressions_filter.Text:=' ';
  expressions_filter.Text:='';
end;

procedure TForm1.operations_filterChange(Sender: TObject);
var
  I: Integer;
  S: String;
  operation_number: integer;
  selected_group : string;
  temp_string: string;
  begin
    operations.Items.BeginUpdate;
    try
      begin
      operations.Clear;
      selected_group:=operations_groups.text;
      //wenn kein filter
      if trim(operations_filter.Text) <> '' then
      begin
        S := operations_filter.Text;
        operation_number:=strtoint(EvaluateXPathExpression('count(//function[contains(translate(., "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz"),"'+S+'")])', Xml.DocumentElement).AsText);
        //showmessage(inttostr(operation_number));
        for I := 1 to operation_number do
        begin
        temp_string:=EvaluateXPathExpression('//function[contains(translate(., "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz"),"'+S+'")]['+inttostr(i)+']/@name', Xml.DocumentElement).AsText;
        if temp_string<>'' then operations.Items.Add(temp_string);
        end;
      end
   else
   begin
       operations.Items.BeginUpdate;
       operations.Clear;
  ////////////////
   if operations_groups.text<>'' then
      XPathResult := EvaluateXPathExpression('count(//group[@name="'+selected_group+'"]//function)', Xml.DocumentElement)
   else
       XPathResult := EvaluateXPathExpression('count(//function)', Xml.DocumentElement);

       operation_number := strtoint(String(XPathResult.AsText));
       for i:=1 to operation_number do
       if operations_groups.text<>'' then
       begin
       temp_string:=trim(EvaluateXPathExpression('(//group[@name="'+selected_group+'"]//function['+inttostr(i)+']/@name)', Xml.DocumentElement).AsText);
       if temp_string<>'' then operations.Items.Add(temp_string);
       end
       else
       begin
       temp_string:=trim(trim(EvaluateXPathExpression('(//function['+inttostr(i)+']/@name)', Xml.DocumentElement).AsText));
       if temp_string<>'' then operations.Items.Add(temp_string);
       end;
  operations.Items.EndUpdate;
   end;
      end;
    finally

      expressions.Sorted:=true;
      statusbar2.SimpleText:=inttostr(operations.Items.Count) + ' operation(s)';
      statusbar1.SimpleText:=inttostr(expressions.Items.Count) + ' expression(s)';
      operations.Items.EndUpdate;
    end;
  end;

procedure TForm1.operations_filterDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=false;
end;

procedure TForm1.operations_filterKeyPress(Sender: TObject; var Key: char);
begin
      if ord(Key)=27 then
      begin
      operations_filter.text:='';
      unselect_all(operations);
      expressions_filter.Text:=' ';
      expressions_filter.Text:='';

      end;
end;

procedure TForm1.PageControl2ContextPopup(Sender: TObject; MousePos: TPoint;
  var Handled: Boolean);
begin

end;

procedure TForm1.PopupMenu1Popup(Sender: TObject);
begin
menuitem22.Enabled:=true;

menuitem3.Enabled:=true;
menuitem4.Enabled:=true;
menuitem6.Enabled:=true;
menuitem21.Enabled:=true;
menuitem5.Enabled:=true;
menuitem28.Enabled:=true;

  case PageControl2.TabIndex of
  0: begin menuitem22.Enabled:=false;
  if pre_list.Count = 0 then
  begin
  menuitem3.Enabled:=false;
  menuitem4.Enabled:=false;
  menuitem6.Enabled:=false;
  menuitem21.Enabled:=false;
  menuitem5.Enabled:=false;
  menuitem28.Enabled:=false;
  end;

  if pre_list.SelCount=0 then begin
  menuitem3.Enabled:=false;
  menuitem4.Enabled:=false;
  menuitem6.Enabled:=false;
  menuitem21.Enabled:=false;
  menuitem5.Enabled:=false;
  end;


  end;
  1: begin menuitem23.Enabled:=false;

  if act_list.Count = 0 then
  begin
  menuitem3.Enabled:=false;
  menuitem4.Enabled:=false;
  menuitem6.Enabled:=false;
  menuitem21.Enabled:=false;
  menuitem5.Enabled:=false;
  menuitem28.Enabled:=false;
  end;

  if act_list.SelCount=0 then begin
  menuitem3.Enabled:=false;
  menuitem4.Enabled:=false;
  menuitem6.Enabled:=false;
  menuitem21.Enabled:=false;
  menuitem5.Enabled:=false;
  end;

  end;
  2: begin menuitem24.Enabled:=false;

  if post_list.Count = 0 then
  begin
  menuitem3.Enabled:=false;
  menuitem4.Enabled:=false;
  menuitem6.Enabled:=false;
  menuitem21.Enabled:=false;
  menuitem5.Enabled:=false;
  menuitem28.Enabled:=false;
  end;

  if post_list.SelCount=0 then begin
  menuitem3.Enabled:=false;
  menuitem4.Enabled:=false;
  menuitem6.Enabled:=false;
  menuitem21.Enabled:=false;
  menuitem5.Enabled:=false;
  end;

  end;
  3: begin menuitem25.Enabled:=false;

  if exp_list.Count = 0 then
  begin
  menuitem3.Enabled:=false;
  menuitem4.Enabled:=false;
  menuitem6.Enabled:=false;
  menuitem21.Enabled:=false;
  menuitem5.Enabled:=false;
  menuitem28.Enabled:=false;
  end;

  if exp_list.SelCount=0 then begin
  menuitem3.Enabled:=false;
  menuitem4.Enabled:=false;
  menuitem6.Enabled:=false;
  menuitem21.Enabled:=false;
  menuitem5.Enabled:=false;
  end;

  end;
  4: begin

  end;
end;
end;

end.

