unit editor;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls, lazutf8, clipbrd, ComCtrls, regexpr, StrUtils;

type

  { TForm2 }

  TForm2 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    Button8: TButton;
    ComboBox1: TComboBox;
    Label1: TLabel;
    ListBox1: TListBox;
    OpenDialog1: TOpenDialog;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    SaveDialog1: TSaveDialog;
    StatusBar1: TStatusBar;
    ve_accept: TButton;
    ve_cancel: TButton;
    ve_line: TComboBox;
    ve_value: TComboBox;
    Panel1: TPanel;
    procedure Button1Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure ComboBox1KeyPress(Sender: TObject; var Key: char);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: char);
    procedure ListBox1DblClick(Sender: TObject);
    procedure ListBox1KeyPress(Sender: TObject; var Key: char);
    procedure ListBox1KeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ve_acceptClick(Sender: TObject);
    procedure ve_cancelClick(Sender: TObject);
    procedure ve_lineEditingDone(Sender: TObject);
    procedure ve_lineKeyPress(Sender: TObject; var Key: char);
    procedure ve_valueEnter(Sender: TObject);
    procedure ve_valueKeyPress(Sender: TObject; var Key: char);
    procedure ve_valueMouseEnter(Sender: TObject);
    procedure ve_valueSelect(Sender: TObject);
  private

  public

  end;

var
  Form2: TForm2;
  value: string = '';
  Sig_List: TStringList;
  Change: boolean = false;
implementation

uses osmae_src;

{$R *.lfm}

{ TForm2 }

procedure TForm2.ve_cancelClick(Sender: TObject);
begin
  value:='';
  editor.Change:=false;
  form2.Close;
end;

procedure TForm2.ve_lineEditingDone(Sender: TObject);
begin
  if ve_line.text='' then
  begin
  statusbar1.Color:=clRed;
  statusbar1.Font.Color:=clRed;
  statusbar1.SimpleText:='Please input valid line!';
  //showmessage('Please input valid line');
  //ve_line.SetFocus;
  end
  else
  begin
  statusbar1.Font.color:=clBlack;
  statusbar1.SimpleText:='';
  end;
end;

procedure TForm2.ve_lineKeyPress(Sender: TObject; var Key: char);
begin
if ord(Key)=27 then form2.Close;
if ord(Key)=13 then if ve_value.Text<>'' then ve_accept.Click else ve_value.SetFocus;
end;

procedure TForm2.ve_valueEnter(Sender: TObject);
begin

end;

procedure TForm2.ve_valueKeyPress(Sender: TObject; var Key: char);
begin
if ord(Key)=27 then form2.Close;
if ord(Key)=13 then ve_accept.Click;
end;

procedure TForm2.ve_valueMouseEnter(Sender: TObject);
begin
//    ve_value.SelLength := 0;
//    ve_value.SelStart:=ve_value.;
end;

procedure TForm2.ve_valueSelect(Sender: TObject);
begin

end;

procedure TForm2.ve_acceptClick(Sender: TObject);
begin
  value:=ve_value.Caption;
  if (trim(value)='') or (utf8pos('$',value)>=1) {or (ve_line.Text='')} then
     showmessage('Empty expressions or not resolved parameters are not allowed!')
  else
  begin
  if ve_value.Items.IndexOf(value)=-1 then ve_value.items.add(value);
  if trim(value)<>'' then
  begin
  if change then
     begin
     with form1 do
     case form1.PageControl2.TabIndex of
       0: if ve_line.text<>'' then form1.pre_list.Items[pre_list.itemindex]:=(ve_line.text+osmae_src.line_sep_marker+' '+value) else form1.pre_list.Items[pre_list.itemindex]:=value;
       1: if ve_line.text<>'' then form1.act_list.Items[act_list.itemindex]:=(ve_line.text+osmae_src.line_sep_marker+' '+value) else form1.act_list.Items[act_list.itemindex]:=value;
       2: if ve_line.text<>'' then form1.post_list.Items[post_list.itemindex]:=(ve_line.text+osmae_src.line_sep_marker+' '+value) else form1.post_list.Items[post_list.itemindex]:=value;
       3: if ve_line.text<>'' then form1.exp_list.Items[exp_list.itemindex]:=(ve_line.text+osmae_src.line_sep_marker+' '+value) else form1.exp_list.Items[exp_list.itemindex]:=value;
     end;
     end
  else
  begin
  with form1 do
  case form1.PageControl2.TabIndex of
    0: if ve_line.text<>'' then form1.pre_list.Items.Add(ve_line.text+osmae_src.line_sep_marker+' '+value) else form1.pre_list.Items.Add(value);
    1: if ve_line.text<>'' then form1.act_list.Items.Add(ve_line.text+osmae_src.line_sep_marker+' '+value) else form1.act_list.Items.Add(value);
    2: if ve_line.text<>'' then form1.post_list.Items.Add(ve_line.text+osmae_src.line_sep_marker+' '+value) else form1.post_list.Items.Add(value);
    3: if ve_line.text<>'' then form1.exp_list.Items.Add(ve_line.text+osmae_src.line_sep_marker+' '+value) else form1.exp_list.Items.Add(value);
  end;
  end;
  end;
  editor.Change:=false;
  form1.menuitem16.Click;
  form2.Close;
  end;
end;

procedure TForm2.FormKeyPress(Sender: TObject; var Key: char);
begin
  if ord(Key)=27 then
  begin
  form2.Close;
  end;
end;

procedure TForm2.ComboBox1KeyPress(Sender: TObject; var Key: char);
begin
  if ord(key)=13 then
  if trim(combobox1.text)<>'' then
  begin
  if Sig_List.IndexOf(combobox1.Text)=-1 then
     Sig_List.Add(combobox1.Text);
  if listbox1.Items.IndexOf(combobox1.text)<0 then
  combobox1.items.add(combobox1.text);
  combobox1.text:='';
  combobox1.OnChange(combobox1);
  end;
  if ord(Key)=27 then
  begin
  combobox1.text:='';
  combobox1.OnChange(combobox1);
  end;
end;

procedure TForm2.FormCreate(Sender: TObject);
begin
    ListBox1.Items.LoadFromFile('list.ini');
    Sig_List.Assign(listbox1.items);
end;

procedure TForm2.FormDestroy(Sender: TObject);
begin
  if listbox1.Items.Count>0 then
    editor.Form2.ListBox1.Items.SaveToFile('list.ini');
end;

procedure TForm2.ComboBox1Change(Sender: TObject);
var
  I: Integer;
  S: String;
begin
  ListBox1.Items.BeginUpdate;
  try
    begin
    ListBox1.Clear;
    if trim(combobox1.Text) <> '' then
    begin
      S := combobox1.Text;
      for I := 0 to Sig_List.Count - 1 do
      begin
        if pos(lowercase(S), lowercase(Sig_List[I]))>0 then
          ListBox1.Items.Add(Sig_List[I]);
      end;
    end
 else
 begin
     ListBox1.Items.BeginUpdate;
     ListBox1.Clear;
     for I := 0 to Sig_List.Count - 1 do
       ListBox1.Items.Add(Sig_List[I]);
     ListBox1.Items.EndUpdate;
 end;
    end;
  finally
    listbox1.Sorted:=true;
    ListBox1.Items.EndUpdate;
  end;
end;

function reg_replace(input_string: string; find_string: string; replace_string: string): string;
var r: TRegExpr;
    sResult: string ='';
begin
         r:= TRegExpr.Create;
    try
        r.Expression := find_string;
        sResult:= r.Replace(input_string,replace_string,true);
        sResult:=trim(DelSpace1(sResult));
    finally
        r.Free;
    end;
    result:=sResult;
end;

procedure TForm2.Button1Click(Sender: TObject);
var insert_text: string = '{None}';
begin
 ve_value.text:=reg_replace(ve_value.Text,'\$1','{None}');
end;

procedure TForm2.Button7Click(Sender: TObject);
begin
  Sig_List.SaveToFile('list.ini');
end;

procedure TForm2.ListBox1DblClick(Sender: TObject);
begin
  if ve_value.SelStart>0 then
  ve_value.Text:=utf8copy(ve_value.text,1,ve_value.SelStart)+listbox1.Items[listbox1.ItemIndex]+utf8copy(ve_value.text,ve_value.SelStart+1,utf8length(ve_value.Text))
  else
  begin
  //ve_value.Text:=listbox1.Items[listbox1.ItemIndex];}
  clipbrd.Clipboard.AsText:=listbox1.Items[listbox1.ItemIndex];
  showmessage('Signal copied to clipboard!')
  end;
end;

procedure TForm2.ListBox1KeyPress(Sender: TObject; var Key: char);
begin
  if ord(Key)=27 then
  begin
  combobox1.Text:='';
  combobox1.OnChange(combobox1);
  end;
end;

procedure TForm2.ListBox1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var cur_pos: integer;
begin
    if Key = 46 then
    begin
    if listbox1.SelCount=1 then
    begin
    cur_pos:=listbox1.ItemIndex;
    sig_list.Delete(sig_list.IndexOf(listbox1.Items[listbox1.ItemIndex]));
    listbox1.Items.Delete(listbox1.ItemIndex);
    if listbox1.Items.Count<>0 then
    listbox1.Selected[cur_pos]:=true;
    end;
    end;
end;

end.


